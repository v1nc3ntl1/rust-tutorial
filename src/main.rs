use std::io;

fn main() {
   loop {
       println!("Choose a program : ");
       println!("  (1) Fibonacci ");
       println!("  (2) Fahrenheit converter ");
       println!("  (3) Print Twelve days of Christmas Lyric");
       println!("Enter (e) and Enter for exit");

       let mut choice = String::new();

       io::stdin().read_line(&mut choice)
           .expect("Unable to select any program. Try again");
    
       let choice = choice.chars().next().unwrap();

       if choice == 'e' || choice == 'E' {
           break;
       } else if choice == '1' {
           fibonacci_program();
       } else if choice == '2' {
           convert_fahrenheit();
       } else if choice == '3' {
           twelve_days_of_christmas();
       } else {
           println!("Invalid options! \n");
           continue;
       }
   }
}

fn fibonacci_program() {
    loop {
        println!("How many fibonacci number to print ?");

        let mut max = String::new();

        io::stdin().read_line(&mut max)
            .expect("Failed to read line.");
        
        let max: u8 = match max.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Enter an integer!");
                continue;
            }
        };

        print_fibonacci(0, 1, 1, max);
        println!("\n");

        break;
    }
}

fn print_fibonacci(prev_1: u32, prev_2: u32, counter:u8, max: u8)
{
    if counter <= max {
        let new_number = prev_1 + prev_2;
        print!("{}, ", new_number);
        print_fibonacci(prev_2, new_number, counter + 1, max);
    }
}

fn convert_fahrenheit() {

    // (100°F − 32) × 5/9 = 37.778°C

    loop {
        println!("Enter Fahrenheit (F): ");

        let mut fahrenheit = String::new();

        io::stdin().read_line(&mut fahrenheit)
            .expect("Unable to readline!");

        let fahrenheit: f32 = match fahrenheit.trim().parse() {
            Ok(floating) => floating,
            Err(err) => {
                println!("Error converting to floating point! {}", err);
                continue;
            }
        };
    
        let celsius = (fahrenheit - 32.0) * 5.0 / 9.0;
        println!("{} Fahrenheit is equal to {} celsius.\n", fahrenheit, celsius);

        break;
    }
}

fn twelve_days_of_christmas() {
    let days = ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth"];
    let num = ["a", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve"];
    let gift = ["partridge in a pear tree", "turtle doves", "French hens", "calling birds", "gold rings", "geese a laying", "swans a swimming", "maids a milking", "ladies dancing", "lords a leaping", "pipers piping", "drummers drumming"];

    //const first_line: &str = "On the {} day of Christmas \nMy true love gave to me";
    const max_days: i8 = 12;

    for counter in 1..13 {
        println!("On the {} day of Christmas \nMy true love gave to me", days[counter - 1]);

        for inner_counter in (1..(counter + 1)).rev() {
            let and_word = {
                if counter == 1 {
                    ""
                } else if inner_counter != 1 {
                    ""
                } else {
                    "And "
                }
            };

            //TO DO - convert first character to upper
            println!("{}{} {}", and_word, num[inner_counter - 1], gift[inner_counter - 1]);
        }

        println!();
    }    
}

fn first_character_upper_case(s: &str) -> String {
    let mut c = s.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
    }
}